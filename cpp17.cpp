﻿#include <iostream>

class Vector
{ 
public : 
    Vector() : x(6), y(3), z(8) 
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double Length() 
    {
        return sqrt(x*x + y*y + z*z);
    }

private :
    double x;
    double y;
    double z;
};
int main()
{
    Vector v;
    std::cout << v.Length();
}